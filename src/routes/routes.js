const { Router } = require("express");
const LojaController =  require("../controllers/LojaController");
const ProdutoController = require("../controllers/ProdutoController");
const ClienteController = require("../controllers/ClienteController");
const router = Router();

//rotas para cliente
router.post("/Cliente", ClienteController.create);
router.get("Cliente /:id", ClienteController.show);
router.get("/Cliente", ClienteController.index);
router.put("/Cliente /:id", ClienteController.update);
router.delete("/Cliente /:id", ClienteController.destroy);

//rotas para produto
router.post("/Produto", ProdutoController.create);
router.get("Produto /:id", ProdutoController.show);
router.get("/Produto", ProdutoController.index);
router.put("/Produto /:id", ProdutoController.update);
router.delete("/Produto /:id", ProdutoController.destroy);
router.put("addRelationship/:id", ProdutoController.addRelationship);
router.delete("removeRelationship/id", ProdutoController.removeRelationship);

//rotas para loja
router.post("/Loja", LojaController.create);
router.get("Loja /:id", LojaController.show);
router.get("/Loja", LojaController.index);
router.put("/Loja /:id", LojaController.update);
router.delete("/Loja /:id", LojaController.destroy);

module.exports = router;
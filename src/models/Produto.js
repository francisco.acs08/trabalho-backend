const DataType =  require("sequelize");
const sequelize = require("../config/sequelize");

const Produto = sequelize.define('Produto', {
    
    nome: { 
        type: DataType.STRING,
        allowNull: false
    },
    
    valor: {
        type: DataType.DOUBLE,
        allowNull: false
    },

    foto: {
        type: DataType.TEXT,
        allowNull: false
    },

    categoria: {
        type: DataType.STRING,
        allowNull: false
    },

    quantidade: {
        type: DataType.INTEGER,
        allowNull: false
    },

    descricao: {
        type: DataType.TEXT,
        allowNull: false
    },

});

Produto.associate = function(models) {
    Produto.belongsTo(models.Cliente);
    Produto.belongsTo(models.Loja);
};

module.exports = Produto;

const DataType =  require("sequelize");
const sequelize = require("../config/sequelize");

const Loja = sequelize.define('Loja', {
    
    nome: { 
        type: DataType.STRING,
        allowNull: false
    },
    
    email: {
        type: DataType.STRING,
        allowNull: false
    },

    telefone: {
        type: DataType.STRING,
        allowNull: false
    },

    });

    Loja.associate = function(models) {
        Loja.hasMany(models.Produto);
    };

    module.exports = Loja;


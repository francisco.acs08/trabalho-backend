const DataType = require("sequelize");
const sequelize = require("../config/sequelize");

const Cliente = sequelize.define('Cliente', {
    nome: {
        type: DataType.STRING,
        allowNull: false
    },

    email: {
        type: DataType.STRING,
        allowNull: false
    },

    cpf: {
        type: DataType.STRING,
        allowNull: false
    },

    dataNascimento: {
        type: DataType.DATEONLY,
    },

    endereco: {
        type: DataType.STRING,
        allowNull: false
    },

    senha: {
        type: DataType.STRING,
        allowNull: false
    },

});

Cliente.associate = function(models) {
    Cliente.hasMany(models.Produto);
};

module.exports = Cliente;



const { response } = require('express');
const { Model } = require('sequelize');
const Cliente = require('../models/cliente');
const Produto = require('../models/Produto');

const create = async(req,res) => {
    try{
        const produto = await Produto.create(req.body);
        return res.status(201).json({message: "Produto cadastrado com sucesso!", produto: produto});
    }catch(err){
        res.status(500).json({error: err});
    }
};

const index = async(req,res) => {
    try {
        const produto = await Produto.findAll();
        return res.status(200).json({cliente});
    }catch(err) {
        return res.status(500).json({err});    
    }
};

const show = async(req,res) => {
    const {id} = req.params;
    try {
        const produto = await Produto.findByPk(id);
        return res.status(200).json({cliente});
    }catch(err) {
        return res.status(500).json({err});
    }
};

const update = async(req,res) => {
    const {id} = req.params;
    try {
        const [updated] = await Produto.update(req.body , {where: {id: id}});
        if(updated) {
            const produto = await Produto.findByPk(id);
            return res.status(200).send(cliente);
        }
        throw new Error();
    }catch(err) {
        return res.status(500).json("Produto não encontrado");
    }
};

const destroy = async(req,res) => {
    const {id} = req.params;
    try {
        const deleted = await Produto.destroy({where: {id: id}});
        if(deleted) {
            return res.status(200).json("Produto deletado com sucesso");
        }
        throw new Error();
    }catch(err) {
        return res.status(500).json("Produto não encontrado.");
    }
};

const addRelationship = async(req,res) => {
    const {id} = req.params;
    try {
        const produto = await Produto.findByPk(id);
        const cliente = await Cliente.findByPk(req.body.ClienteId);
        await Produto.setCliente(cliente);
        return res.status(200).json(produto);
    } catch(err) {
        return res.status(500).json({err});
    }
};

const removeRelationship = async(req,res) => {
    const {id} = req.params;
    try {
        const produto = await Produto.findByPk(id);
        await Protudo.setUser(null);
        return res.status(200).json(produto);
    }catch(err) {
        return res.status(500).json({err});
    }
};

module.exports = {
    create,
    index,
    show,
    update,
    destroy,
    addRelationship,
    removeRelationship
};